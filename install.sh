#!/usr/bin/env bash
cp ./src/setupMain.sh ./src/main/setup.sh
cp ./src/setupWarehouse.sh ./src/warehouse/setup.sh

docker-compose up -d && echo "Please wait while service is up..." && \
sleep 5 && winpty docker exec -ti main-web sh setup.sh && echo "Please wait while main is setting up"
sleep 5 && winpty docker exec -ti warehouse-web sh setup.sh && echo "All done"