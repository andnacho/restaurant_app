<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/pedidos', 'PedidosController@store')->name('pedidos.store');
Route::post('/pedidos/nodispatched', 'PedidosController@showAllNotDispached')->name('pedidos.undispatched');
Route::post('/pedidos/dispacher', 'PedidosController@update')->name('pedidos.update');
Route::post('/pedidos/total', 'PedidosController@showAll')->name('pedidos.update');



