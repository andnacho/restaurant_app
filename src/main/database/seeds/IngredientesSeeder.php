<?php

use Illuminate\Database\Seeder;

class IngredientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->createIngredient('Tomato');
      $this->createIngredient('Lemon');
      $this->createIngredient('Potato');
      $this->createIngredient('Rice');
      $this->createIngredient('Ketchup');
      $this->createIngredient('Lettuce');
      $this->createIngredient('Onion');
      $this->createIngredient('Cheese');
      $this->createIngredient('Meat');
      $this->createIngredient('Chicken');

    }

    private function createIngredient($name){
       $ing = \App\Ingrediente::create([
            'name' => $name,
            'quantity' => 5
        ]);

       $ing->save();
    }
}
