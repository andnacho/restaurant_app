//Charging total purchases
$('#totalPurchases').click(function () {

    $.ajax({
        type: "POST",
        url: urlapi + "/api/purchases/total"
    }).done(function (dataPurchases) {

        retrievesIngredientRecords(dataPurchases);

    }).fail(function (data) {
        alert('System is not working')
    });
});

//Retrieves ingredient and then call insertPurchaseInformation
function retrievesIngredientRecords(dataPurchases) {
    $.ajax({
        type: "POST",
        url: urlapi + "/api/ingredientes"
    }).done(function (dataIngredients) {

        $('#purchases_table tbody, #tpurchases span').empty();
        $('#tpurchases span').append(dataPurchases.length);

        insertPurchaseInformation(dataPurchases, dataIngredients);

    }).fail(function (data) {
        alert('System is not working')
    });
}

//Variable user for saving the ingredient sum of purchases
var ingredientsSum = {
    Tomato: 0,
    Rice: 0,
    Potato: 0,
    Onion: 0,
    Meat: 0,
    Lettuce: 0,
    Lemon: 0,
    Ketchup: 0,
    Chicken: 0,
    Cheese: 0
};

//Insert purchase information in the modal purchases
function insertPurchaseInformation(purchases, ingredients) {

    reinitialize();
    $('#purchases_table_total tbody').empty();

    $.each(purchases, function (keyP, valueP) {

        $.each(ingredients, function (keyI, valueI) {
            if (valueI.id === valueP.ingrediente_id) {

                sumValues(valueI.name, valueP.quantity);

                $('#purchases_table tbody').append(
                    '<tr>' +
                    '<td>' + valueI.name + '</td>' +
                    '<td>' + valueP.quantity + '</td>' +
                    '<td>' + valueP.created_at + '</td>' +
                    '</tr>');
            }
        })
    });
    $.each(ingredientsSum, function (keyS, valueS) {

        if (valueS > 0) {
            $('#purchases_table_total tbody').append(
                '<tr>' +
                '<td>' + keyS + '</td>' +
                '<td>' + valueS + '</td>' +
                '</tr>');
        }
    });
}

function sumValues(ingredient, quantity) {

    ingredientsSum[ingredient] = ingredientsSum[ingredient] + quantity;

}

function reinitialize() {
    ingredientsSum.Tomato = 0;
    ingredientsSum.Rice = 0;
    ingredientsSum.Potato = 0;
    ingredientsSum.Onion = 0;
    ingredientsSum.Meat = 0;
    ingredientsSum.Lettuce = 0;
    ingredientsSum.Lemon = 0;
    ingredientsSum.Ketchup = 0;
    ingredientsSum.Chicken = 0;
    ingredientsSum.Cheese = 0;
}

//Dispatching product if there are enought ingredients
//Else it will do a request to
//Inventory laravel aplication for buying new product
function dispatchDish(orderId, dishId, element) {

    element.setAttribute("disabled", "disabled");
    element.innerHTML = '<i class="fas fa-spinner fa-spin"></i>';

    $.ajax({
        data: {'orderId': orderId, 'dishId': dishId},
        type: "POST",
        url: urlapi + "/api/ingredientes/inventory"
    }).done(function (data) {

        //Verify if it is necessary some ingredient
        if (data[0].length > 0) {
           changeButton(orderId, dishId, element, data);
        } else {
            takeFromInventory(dishId);
            updateDish(orderId);
        }
    });
}

//Function to change botton when there's not enought ingredients
function changeButton(orderId, dishId, buttonInfo, data) {

    var btnParent = buttonInfo.parentElement;
    btnParent.innerHTML = '<button type="button" onclick="askForIngredients(' + orderId + ',' + dishId + ',\'' + data[0] + '\'' + ',\'' + data[1] + '\',this)" ' +
        'class="btn btn-danger" data-toggle="modal" data-target="#warehouse_box">' +
        '<p style="font-size: small; margin:0">Ask for buying</p><p style="font-size: small; margin:0">ingrendiens lacking</p>' +
        '</button>';
}

//Insert an Iframe from the inventory app to the modal,
function askForIngredients(orderId, dishId, ingredients, quantities, buttonInfo) {
    console.log(ingredients + ' ' + quantities);
    $('#warehouse').empty();
    $('#warehouse').append('<iframe src="' + urlapi + '/purchases?ingredients=' + ingredients + '&quantities=' + quantities +
        '" marginwidth="0" marginheight="0" name="ventana_iframe" scrolling="yes" border="0" frameborder="0" style="width:100%" height="300">' +
        '</iframe>')
}



//Calling ajax to inventory
$('#inventory').click(function () {

    chargeInventory();

    //Uncomment for use automatic charge of inventory
    // var refresh = setInterval(function () {
    //     chargeInventory();
    // }, 3000);
});

//charging inventory
function chargeInventory() {
    $.ajax({
        type: "POST",
        url: urlapi + "/api/ingredientes"
    }).done(function (data) {

        $('#inventory_table tbody').empty();
        $.each(data, function (key, value) {

            $('#inventory_table tbody').append(
                '<tr>' +
                '<td>' + value.name + '</td>' +
                '<td>' + value.quantity + '</td>' +
                '</tr>');
        });
    });
}

function takeFromInventory(dishId) {
    $.ajax({
        data: {'dishId': dishId},
        type: "POST",
        url: urlapi + "/api/ingredientes/taking"
    }).done(function (data) {

    }).fail(function () {
        alert('The system is not working')
    });
}

function updateDish(orderId) {
    $.ajax({
        data: {'orderId': orderId},
        type: "POST",
        url: "/api/pedidos/dispacher"
    }).done(function (data) {

        chargeKitchen();

    }).fail(function () {
        alert('The system is not working aaa')
    });
}


