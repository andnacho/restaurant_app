chargeKitchen();


//When pressing button ordering, it sends the order
$('#ordering').click(function () {
    orderSend();
});

//When pressing enter in quantity, it sends the order
$('#quantity').keypress(function (e) {
    if(e.which == 13) {
        orderSend();
    }
});

//Function use to send the order
function orderSend() {
    $.ajax({
        data: {'quantity': $('#quantity').val()},
        type: "POST",
        url: "/api/pedidos"
    }).done(function (data) {

        chargeKitchen();

    });
}

//charging total Orders
$('#totalOrders').click(function () {

    $.ajax({
        type: "POST",
        url: "/api/pedidos/total"
    }).done(function (data) {

        $('#sumOrders tbody, #torders span, #sumOrders').empty();

        $('#torders span').append(data.length);

        $.each(data, function (key, value) {

            $('#sumOrders').append(
                '<tr>' +
                '<td>' + value.dish_name + '</td>' +
                '<td>' + (value.dispatched ? "Yes": "No") + '</td>' +
                '</tr>');
        });

    }).fail(function (data) {
        console.log('cierre')
    });
});


//charging kitchen table
function chargeKitchen() {
    $.ajax({
        type: "POST",
        url: "/api/pedidos/nodispatched"
    }).done(function (data) {

        $('#orders tbody').empty();
        data.forEach(function (element) {

            $('#orders tbody').append('<tr><td>' + element.dish_name +
                '</td><td>' +
                '<button onclick="dispatchDish(' + element.id + ',' + element.dish_id + ', this)" ' +
                'class="btn btn-success">' +
                '<p style="font-size: small; margin:0">Ask for ingredients</p><p style="font-size: small; margin:0">and dispatch order</p>' +
                '</button>' +
                '</td></tr>')
        });

    }).fail(function () {
        alert('The system is not working')
    });
}


