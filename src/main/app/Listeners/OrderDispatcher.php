<?php

namespace App\Listeners;

use App\Events\OrderReceived;
use App\Ingrediente;
use App\Pedido;
use Facades\App\Recipes;
use http\Client\Response;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;

/**
 * Class OrderDispatcher used for changing the dispatched attribute
 * from a "pedido" in the "pedidos" tables to true
 *
 * @package App\Listeners
 */
class OrderDispatcher
{

    public $missIngredients = [];
    public $missIngredientsTotal = [];

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrderReceived $event
     * @return $this->missIngredients
     */
    public function handle(OrderReceived $event)
    {
        $orders = Pedido::all();

//        sleep(3);
        foreach ($event->pedidos as $pedido) {
            $this->missIngredients = [];

            $order = $orders->find($pedido);

            if ($this->verifyIngredientsQuantity($order->dish_id)) {
                if ($this->missIngredients == []) {
//                    $order->dispatched = true;
//                    $order->update();
                }
            }

        }
        return $this->missIngredientsTotal;
    }

    public function verifyIngredientsQuantity($pedido)
    {
        $respuesta = true;
        $recipeIngredients = $this->getRecipe($pedido)['ingredients'];

        $ingredientes = Ingrediente::all();

        foreach ($recipeIngredients as $ingredient => $value) {
            $ing = $ingredientes->where('name', $ingredient);

            if ($ing->pluck('quantity')->first() < $value) {
                $this->missIngredients[] = $ingredient;
                $this->missIngredientsTotal[] = $ingredient;
            }
        }
        return $respuesta;
    }


    public function getRecipe($pedido)
    {
        $recetas = Recipes::giveAllRecipes();

        $selector = 1;

        foreach ($recetas as $recipe) {
            if ($selector === $pedido) return $recipe;
            ++$selector;
        }
    }
}
