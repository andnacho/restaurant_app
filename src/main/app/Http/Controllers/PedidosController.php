<?php

namespace App\Http\Controllers;

use App\Pedido;
use Facades\App\Recipes;
use Illuminate\Http\Request;
use mysql_xdevapi\Collection;

class PedidosController extends Controller
{

    private $pedidosId = [];


    /**
     * Return the view with the list of recipes
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $recipes = Recipes::giveAllRecipes();

        return view('restaurant', compact('recipes'));
    }

    public function showAllNotDispached()
    {
        $order = Pedido::where('dispatched', false)->get();

        return response()->json($order);
    }


    /**
     * Store dish/dishes into "pedidos" table
     * @return response json
     */
    public function store(Request $request)
    {
        $request->validate([
            'quantity' => 'required|numeric'
        ]);

        //Counter for adding dishes
        for ($i = 0; $i < $request->quantity; $i++) {

            $dish = Recipes::giveRecipe();
            $pedido = new Pedido([
                'dish_id'    => $dish['index'],
                'dish_name'  => $dish['name'],
                'dispatched' => false,
            ]);

            $pedido->save();
        }
    }

    /**
     *Update the order dispatched status to true
     *
     */
    public function update()
    {

        $orderId = request('orderId');

        $order = Pedido::find($orderId);
        $order->dispatched = true;
        $order->update();

    }

    /**
     * @return Collection of orders descending
     */
    public function showAll()
    {

        $orders = Pedido::orderBy('id', 'desc')->get();
        return $orders;
    }

}

