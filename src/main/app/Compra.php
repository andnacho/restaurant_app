<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $guarded = [];

    public function proveedor()
    {
        return $this->hasOne(Proveedor::class);
    }

    public function ingredientes()
    {
        return $this->hasOne(Ingrediente::class);
    }
}
