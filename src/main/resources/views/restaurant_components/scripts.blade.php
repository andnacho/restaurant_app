
{{-- External Scripts--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

{{-- url for api warehouse --}}
<script>var urlapi = '{{ env('API_WAREHOUSE_DOMAIN', 'http://localhost:8000') }}'</script>

{{-- Internal Scripts--}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/apilocal.js') }}"></script>
<script src="{{ asset('js/apiwarehouse.js') }}"></script>
