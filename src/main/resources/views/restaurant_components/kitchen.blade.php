<div class="content mx-auto pl-5">
    <div class="title m-b-md text-center">
        <h2 class="mb-5">Kitchen Interface</h2>
        <div class="container m-auto" style="background-color: rgba(250,255,241,0.36); overflow-y: scroll; font-size: 1em; height: 300px; box-shadow: 1px 1px 3px black">
            <table class="table table-striped px-5 small" id="orders">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
        </div>
    </div>
</div>
