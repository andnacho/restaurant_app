<footer>
    <div class="align-bottom text-white p-5" style="height: 8em; background-color: #1b4b72; box-shadow: 1px 1px 2px #1b1e21">
        <h2 class="d-none d-sm-block">
            Developed by Andrés Ignacio Castrillón González
        </h2>
        <h2 class="d-sm-none" style="font-size: 1em;">
            Developed by Andrés Ignacio Castrillón González
        </h2>
    </div>
    <div class="container-fluid align-bottom" style="background-color: #facc22; height: 20px"></div>

</footer>