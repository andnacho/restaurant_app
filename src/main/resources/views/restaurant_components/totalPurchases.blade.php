<div class="modal fade bd-example-modal-lg" id="totalIngredientBought_box" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bd-highlight">
                <h3 id="tpurchases" class="modal-title flex-fill">Total purchases: <span></span></h3>
                <div class="float-right ml-5">
                    <div class="btn-group btn-group-toggle">
                            <button class="btn btn-secondary active" name="options" id="details" autocomplete="off" checked> Details</button>
                            <button class="btn btn-secondary" name="options" id="individualDetails" autocomplete="off"> Individual Details</button>
                    </div>

                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table class="table" id="purchases_table_total">
                    <thead class="thead-dark">
                    <tr>
                        <th>Ingredients</th>
                        <th>Quantity bougth</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

                <table class="table" id="purchases_table">
                    <thead class="thead-dark">
                    <tr>
                        <th>Ingredients</th>
                        <th>Quantity bougth</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>
