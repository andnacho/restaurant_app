<div class="content mx-auto pl-5">
    <h2 class="mb-5 text-center">Administrative Interface</h2>
    <div class="container row mt-5 pb-5 m-sm-auto"
         style="box-shadow: 1px 1px 3px black; height: 300px; background-color: #b3c9d4">
        <div class="form-group col-12 mt-3">
            <label for="">Dishes quantity</label>
            <input type="text"
                   class="form-control" name="quantity" id="quantity" aria-describedby="helpId" value="1">
        </div>

        <input class="btn btn-primary m-1 col" type="button" value="Order it" id="ordering">

        <input class="btn btn-secondary m-1 col" type="button" value="Inventory" id="inventory"
               data-toggle="modal" data-target="#inventory_box">

        <input class="btn btn-info m-1 col" type="button" value="Recipes" id="recipes" data-toggle="modal"
               data-target="#recipes_box">


        <input
                class="btn m-1 col-12"
                type="button"
                value="Total orders" id="totalOrders"
                data-toggle="modal"
                data-target="#totalOrder_box"
                style="background-color: #8f9044; color:white; border-right-width: 10px; right: 4px;">

        <input
                class="btn m-1 col-12"
                type="button"
                value="Total purchases" id="totalPurchases"
                data-toggle="modal"
                data-target="#totalIngredientBought_box"
                style="background-color: #2a9055; color:white; border-right-width: 10px; right: 4px;">

    </div>
</div>