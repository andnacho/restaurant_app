<div class="modal fade" id="inventory_box" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Inventory</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="inventory_table" class="m-auto table table-striped">
                    <thead>
                    <tr>
                        <td>Name</td>
                        <td>Quantity</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>


                </table>
            </div>
        </div>
    </div>

</div>