<div class="modal fade bd-example-modal-lg" id="recipes_box" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Recipes</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                @foreach ($recipes as $recipe)

                    <div class="row mb-2">
                        <h4 class="col-12 mt-3">{{$recipe['name']}}</h4>

                        <div class="col-6">Ingredients</div>
                        <div class="col-6">Quantity required</div>

                        <div class="mt-3 col-12 row">
                            @foreach ($recipe['ingredients'] as $ingredient => $quantity)

                                <div class="col-6">{{$ingredient}}</div>
                                <div class="col-6 pl-4">{{$quantity}}</div>
                            @endforeach
                        </div>
                    </div>

                    <hr>

                @endforeach

            </div>
        </div>
    </div>

</div>

</div>