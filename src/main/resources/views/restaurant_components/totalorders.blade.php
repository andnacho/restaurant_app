<div class="modal fade bd-example-modal-lg" id="totalOrder_box" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="torders" class="modal-title">Total orders: <span></span></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="overflow-y: scroll">

                <table class="table" id="inventory_table">
                    <thead class="thead-dark">
                    <tr>
                        <th>Dish name</th>
                        <th>Dispached</th>

                    </tr>
                    </thead>
                    <tbody id="sumOrders">

                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>

</div>