<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Restaurant Event</title>

{{-- styles --}}
    @include('restaurant_components.styles')
</head>
<body>


<div class="flex-center position-ref full-height" style="margin-bottom: -10em">

    @include('restaurant_components.header')

    <div class="row mb-5" style="margin-top: 4em">

        <div class="float-left mb-5 text-center col">

            @include('restaurant_components.kitchen')

        </div>

        <div class="float-left ml-5 mr-4 mt-0 text-center col">

            @include('restaurant_components.administrative')

        </div>
    </div>
    @include('restaurant_components.footer')
</div>

{{-- Inventory--}}
@include('restaurant_components.inventory')

{{-- Recipes --}}
@include('restaurant_components.recipes')

{{-- Total orders --}}
@include('restaurant_components.totalorders')

{{-- Total purchases --}}
@include('restaurant_components.totalPurchases')

{{-- iframe --}}
@include('warehouse')

{{-- Scripts--}}
@include('restaurant_components.scripts')

</body>
</html>
