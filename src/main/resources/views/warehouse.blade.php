<div class="modal fade bd-example-modal-lg" onblur="chargeKitchen()" id="warehouse_box" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" onclick="chargeKitchen()" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div id="warehouse"></div>

            </div>
        </div>
    </div>

</div>
