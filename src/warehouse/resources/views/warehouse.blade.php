<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Warehouse</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


</head>
<body>
<div class="container">
    <div class="row">

        <h1 class="col-12 mb-4">Ingredients for Buying</h1>
        <table class="table table-striped">
            <tr>
                <th>
                    Ingredient Name
                </th>
                <th>
                    Action
                </th>
                <th>
                    Quantity required
                </th>
            </tr>

            @for ($i = 0; $i < $cicle; $i++)

                <tr id="{{strtolower($ingredients[$i])}}">
                    <td>
                        {{$ingredients[$i]}}
                    </td>
                    <td>
                        <button class="btn btn-success" onclick="buyIt('{{ strtolower($ingredients[$i])}}', this)">Buy
                            ingredient
                        </button>
                    </td>
                    <td>
                        {{$quantities[$i]}}
                    </td>
                </tr>
            @endfor

        </table>

    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>


    function buyIt(ingredient, btnInfo) {

        btnInfo.setAttribute("disabled", "disabled");
        btnInfo.innerHTML = '<i class="fas fa-spinner fa-spin"></i>';

        $.ajax({
            data: {'ingredient': ingredient},
            type: "GET",
            url: "https://recruitment.alegra.com/api/farmers-market/buy"
        }).done(function (data) {
            var quantity = data.quantitySold;
            if (quantity > 0) {
                goBuying(quantity, ingredient, btnInfo);
            } else {
                btnInfo.removeAttribute("disabled", "disabled");
                btnInfo.innerHTML = 'Not available, buy again';
            }

        }).fail(function () {
            // buyIt(ingredient)
        });
    }

    function goBuying(quantity, ingredient, btnInfo) {
        $.ajax({
            data: {'ingredient': ingredient, 'quantity': quantity},
            type: "POST",
            url: "/api/purchases"
        }).done(function (data) {

            var row = btnInfo.parentElement.parentElement;
            if (row.parentElement.childElementCount == 2) {
                row.innerHTML = `
                  <tr>
                    <td>
                        No more ingredients required.
                    </td>
                </tr>
                `;
            } else $("#" + ingredient).remove();


        }).fail(function () {
            // buyIt(ingredient)
        });
    }

</script>
</body>
</html>
