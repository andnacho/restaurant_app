<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    //
    protected $guarded = [];

    public function compra()
    {
        return $this->hasOne(Compra::class);
    }

}
