<?php

namespace App\Http\Controllers;

use App\Compra;
use App\Ingrediente;
use Illuminate\Http\Request;

class ComprasController extends Controller
{

    public function index()
    {

        $ingredients = explode(',', request('ingredients'));
        $quantities = explode(',', request('quantities'));
        $cicle = count($ingredients);

        return view('warehouse', compact('ingredients', 'quantities', 'cicle'));
    }

    public function showAll()
    {
        $purchases = Compra::orderBy('id', 'desc')->get();


        return response($purchases)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Content-Type', 'application/json');
    }


    public function buyProduct()
    {
        $quantity = request('quantity');
        $product = request('ingredient');

        $productCap = ucfirst($product);


        $compra = new Compra([
            "proveedor_id"   => 1,
            "ingrediente_id" => Ingrediente::where('name', $productCap)->first()->id,
            'quantity'       => $quantity
        ]);
        $compra->save();

        return 'Compra éxitosa';
    }


}
