<?php

namespace App\Http\Controllers;

use App\Ingrediente;
use App\Pedido;
use App\Recipes;
use Illuminate\Http\Request;

class IngredienteController extends Controller
{

    public $missIngredients = [];

    //this variable will be sent to say if there some ingrentients required
    public $missIngredientsTotal = [];
    //this variable will sent the quantity required for each ingredient
    public $quantityMissIngredients = [];

    //Give the list of the ingredients on stock
    public function index()
    {
        $ingredient = Ingrediente::all();
        return response($ingredient)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Content-Type', 'application/json');
    }

    //Takes from inventory
    public function takeFromInventory()
    {
        $id = request('dishId');
        $dishId = (int) $id;
        $ingredients = Ingrediente::all();
        $recipe = $this->getRecipe($dishId);

        foreach ($recipe['ingredients'] as $ingredient => $value) {
            $newIngredientQuantity = $ingredients->where('name', $ingredient)->first();
            $newIngredientQuantity->quantity = $newIngredientQuantity->quantity - $value;
            $newIngredientQuantity->update();

        }

        return response('Ingredients dispatched', 200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Content-Type', 'text/plain');
    }

    /**
     * Verify if there are the necesary ingredients
     * @return array
     */
    public function veriryingQuantity()
    {
        $dishId = request('dishId');

        $this->verifyIngredientsQuantity($dishId);

        return response()
            ->json([$this->missIngredientsTotal, $this->quantityMissIngredients])
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Content-Type', 'application/json');

    }


    /**
     * @param $dishId
     * @return bool
     */
    public function verifyIngredientsQuantity($dishId)
    {

        $recipeIngredients = $this->getRecipe((int) $dishId)['ingredients'];
        $ingredientes = Ingrediente::all();

        foreach ($recipeIngredients as $ingredient => $value) {
            $ing = $ingredientes->where('name', $ingredient);
            if ($ing->pluck('quantity')->first() < $value) {

                $this->missIngredients[] = $ingredient;
                $this->missIngredientsTotal[] = $ingredient;
                $this->quantityMissIngredients[] = $value - $ing->pluck('quantity')->first();

            }
        }

    }

    /**
     * Search the dish on the class Recipes
     * @param $dishId
     * @return mixed
     */
    public function getRecipe($dishId)
    {
        $recetas = \Facades\App\Recipes::giveAllRecipes();
        $selector = 1;

        foreach ($recetas as $recipe) {

            if ($selector === $dishId) return $recipe;
            ++$selector;
        }
    }
}
