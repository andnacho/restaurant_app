<?php

namespace App;

class Recipes
{
    protected $recipes = [];

    /**
     * Recetas constructor.
     */
    public function __construct()
    {
        $this->recipes = [
            'Onions_soup'                     => $this->onionsSoupeIngredients(),
            'Caesar_salad'                    => $this->caesarSaladIngredients(),
            'Mix_risotto'                     => $this->risottoIngredients(),
            'French_fries_with_melted_cheese' => $this->frenchFriesIngredients(),
            'Steak_with_potato_puree'         => $this->steakPotatoPureeIngredients(),
            'House_salad'                     => $this->houseSaladIngredients()
        ];
    }

    //Functions for getting the ingredients as if it was a database

    private function onionsSoupeIngredients()
    {
        return [
            'index'       => 1,
            'name'        => 'Onions soup',
            'ingredients' => [
                'Onion'  => 2,
                'Cheese' => 1,
                'Potato' => 2,
            ]
        ];
    }

    private function caesarSaladIngredients()
    {
        return [
            'index'       => 2,
            'name'        => 'Caesar salad',
            'ingredients' => [
                'Lettuce' => 2,
                'Onion'   => 1,
                'Cheese'  => 1,
                'Chicken' => 2,
                'Tomato'  => 1,
            ]
        ];
    }

    private function risottoIngredients()
    {
        return [
            'index'       => 3,
            'name'        => 'Mix risotto',
            'ingredients' => [
                'Rice'    => 2,
                'Cheese'  => 1,
                'Meat'    => 2,
                'Chicken' => 1,
            ]
        ];
    }

    private function frenchFriesIngredients()
    {
        return [
            'index'       => 4,
            'name'        => 'French fries with melted cheese',
            'ingredients' => [
                'Potato'  => 2,
                'Ketchup' => 1,
                'Cheese'  => 2,
            ]
        ];
    }

    private function steakPotatoPureeIngredients()
    {
        return [
            'index'       => 5,
            'name'        => 'Steak with potato puree',
            'ingredients' => [
                'Potato' => 2,
                'Lemon'  => 1,
                'Meat'   => 2,
            ]
        ];
    }

    private function houseSaladIngredients()
    {
        return [
            'index'       => 6,
            'name'        => 'House salad',
            'ingredients' => [
                'Tomato'  => 2,
                'Lemon'   => 1,
                'Lettuce' => 2,
                'Onion'   => 2,
                'Cheese'  => 2,
            ]
        ];
    }


    /**
     * Return a random recipe
     * @param null $randomNumber 0-5
     * @return mixed $recipe
     */
    public function giveRecipe($randomNumber = null)
    {
        if(!isset($randomNumber))
        $randomNumber = rand(0, 5);

        $selector = 0;

        foreach ($this->recipes as $recipe) {
            if ($selector == $randomNumber) return $recipe;
            ++$selector;
        }
    }


    public function giveAllRecipes()
    {
        return $this->recipes;
    }


}