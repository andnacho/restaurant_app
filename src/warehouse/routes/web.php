<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/purchases', 'ComprasController@index');

//Route::get('/purchases/total', 'ComprasController@showAll');
//Route::get('/ingredientes/inventory', 'IngredienteController@veriryingQuantity')->name('pedidos.verify');

