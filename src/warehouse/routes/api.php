<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/purchases', 'ComprasController@buyProduct');
Route::post('/purchases/total', 'ComprasController@showAll');

Route::post('/ingredientes', 'IngredienteController@index')->name('ingredientes.index');
Route::post('/ingredientes/inventory', 'IngredienteController@veriryingQuantity')->name('pedidos.verify');
Route::post('/ingredientes/taking', 'IngredienteController@takeFromInventory')->name('ingredientes.take');
