#Restaurant app

This is a one page project use for helping a restaurant's owner 
to manage an event called "*Let's eat toguether's day*"
implementing docker, that means all its request are doing through ajax request
except for buying ingredients, this is processed by a warehouse iframe windows.

##Prerequisites
Install:
* [Docker](https://docs.docker.com/install/)
* [Docker-compose](https://docs.docker.com/compose/install/) 
* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

##Getting started 
###First

* Clone the project located in "*https://andnacho@bitbucket.org/andnacho/* [*restaurant_app.git*](https://andnacho@bitbucket.org/andnacho/restaurant_app.git)"
 
 
*  Before running install.sh who is responsible to setting up the necessary for running "Restaurant app",
it is necessary to assign the global variable "API_WAREHOUSE_DOMAIN" with the domain and port of the host used for app-warehouse service.

```
The variable API_WAREHOUSE_DOMAIN is located on docker-compose.yml in app-main service, 
additionally you can chanche the APP_DEBUG and APP_ENV in these document, 
by default, these variables are setted for production environment, same for app-warehouse.
```
    app-main:
        environment:
             - API_WAREHOUSE_DOMAIN="http://192.168.99.101:8888"
             - APP_DEBUG=false
             - APP_ENV=production
            
    app-warehouse:
         environment:
              - APP_DEBUG=false
              - APP_ENV=production
              
```
Or, if you prefer you could modify the files .env.example of src/main and src/warehouse, 
due to these two files will be copy to its projects as .env files
```

* it is possible that you must remove the "winpty" command on the install.sh file, it depends on the environment you use.

```
    
docker-compose up -d && echo "Please wait while service is up..." && \
sleep 5 && winpty docker exec -ti main-web sh setup.sh && echo "Please wait while main is setting up"
sleep 5 && winpty docker exec -ti warehouse-web sh setup.sh && echo "All done"
``` 

* Then, just execute the install.sh and that's it!. 

##Author
- Andres Ignácio Castrillón G.

##Built With

- [Laravel framework](https://laravel.com/)
- [Docker](https://www.docker.com/)
- [mysql](https://www.mysql.com/)

##Acknowledgments

- This application is a required test developed by an Andres Ignácio Castrillón G. as part of his 
selection process 
